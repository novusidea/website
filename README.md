# Website

## Install dependencies
```bash
nvm use && npm install
```

## Serve with hot reload at [localhost:3000](http://localhost:3000)
```bash
npm run dev
```

## Build for production and launch server
```bash
npm run build
npm run start
```

## Generate static project
```bash
npm run generate
```

## Deployment via sFTP

```bash
cp .env .env.example
```

```
SFTP_HOSTNAME="ftp.example.com"
SFTP_USERNAME="johndoe"
SFTP_PASSWORD="j0hnd0e"
SFTP_REMOTE_ROOT="/absolute/path/to/remote/root"
```

```bash
npm run deploy
```

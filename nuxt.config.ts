const baseUrl = process.env.BASE_URL || 'http://localhost:3000';

const lang = 'de';
const title = 'NOVUS IDEA';
const description = 'I\'m a full-stack web developer from northern Germany.';

export default defineNuxtConfig({
  runtimeConfig: {
      public: {
          title: title,
      },
  },

  ssr: false,

  app: {
      head: {
          title: title,
          htmlAttrs: { lang: lang},
          meta: [
              { charset: 'utf-8' },
              { name: 'viewport', content: 'width=device-width, initial-scale=1' },
              { name: 'format-detection', content: 'telephone=no' },
              { name: 'google-site-verification', content: 'n3AIS_U4nmwRiQpCCSD29ZHmjiz1IzZx7IOifmrGwzA' },
              { name: 'description', content: description },
              { property: 'og:url', content: baseUrl },
              { property: 'og:type', content: 'website' },
              { property: 'og:title', content: title },
              { property: 'og:description', content: description },
              { property: 'og:image', content: baseUrl + '/og-image.webp' },
              { property: 'og:image:type', content: 'image/webp' },
              { property: 'og:image:width', content: '1200' },
              { property: 'og:image:height', content: '630' },
          ],
          link: [
              { rel: 'icon', type: 'image/x-icon', href: baseUrl + '/favicon.ico' },
              { rel: 'icon', type: 'image/x-icon', href: baseUrl + '/favicon.svg' },
          ],
          script: [
              {
                  'src': 'https://analytics.novusidea.de/script.js',
                  'data-website-id': '082f728d-78af-40ea-bc14-df57b539fbf5',
                  'data-domains': 'novusidea.de',
                  'data-do-not-track': true,
                  'async': true,
                  'defer': true,
              }
          ],
      },
  },

  css: ['@/assets/scss/main.scss'],

  experimental: {
      payloadExtraction: false,
  },

  compatibilityDate: '2024-08-29',
})
